//
//  DetailViewController.swift
//  AssetTVdemo
//
//  Created by Anan Mallik on 10/11/2016.
//  Copyright © 2016 Anan Mallik. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController
{

    //Label variables:
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var descText: UILabel!
    @IBOutlet weak var timeText: UILabel!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var termsText: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setValues()

    }

    
    
    
    func setValues()
    {
        
        let defaults = UserDefaults.standard
        
        titleText.text = defaults.string(forKey: "Title")
        descText.text = defaults.string(forKey: "Desc")
        dateText.text = defaults.string(forKey: "Date")
        timeText.text = defaults.string(forKey: "Time")
        termsText.text = defaults.string(forKey: "Terms")
        
        let url = defaults.string(forKey: "Image")
        
        let imgURL: URL = URL(string: url!)!
        let request: URLRequest = URLRequest(url: imgURL)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func display_image()
                {
                    self.image.image = UIImage(data: data!)
                }
                
                DispatchQueue.main.async
                    {
                        display_image()
                }
            }
            
        })
        
        
        task.resume()
    }
    
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()

    }
    

}
