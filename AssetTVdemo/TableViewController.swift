//
//  TableViewController.swift
//  AssetTVdemo
//
//  Created by Anan Mallik on 08/11/2016.
//  Copyright © 2016 Anan Mallik. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    
    @IBOutlet weak var tableView: UITableView!
    
    //Array variables to store data from JSON:
    var TableData0:[String] = []//Title
    var TableData1:[String] = []//Image URL
    var TableData2:[String] = []//Terms
    var TableData3:[String] = []//Duration
    var TableData4:[String] = []//Date
    var TableData5:[String] = []//Description
    
    var rowNum = 0
    
    //Main URL to retreive JSON data from API:
    let url = "https://embed-titan-uk.pantheonsite.io/api/channel-json/latest/2240/23"

    override func viewDidLoad()
    {
        
        load_JSON(url)
        
        tableView.allowsSelection = true
        
        super.viewDidLoad()
        
    }
    
    //Parse the JSON data and store into separate Arrays for each: Title, Terms, Image URl, duration etc.
    func load_JSON(_ url:String)
    {
        
        let requestURL: URL = URL(string: url)!
        
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: requestURL)
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest as URLRequest)
        {
            (data, response, error) -> Void in
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! NSArray
                
                    
                //Initialize dictionary variable:
                var dict = json[0] as! NSDictionary
                
                
                for i in 0...100
                {
                    dict = json[i] as! NSDictionary
                    //Add data from JSON to array:
                    self.TableData0.append((dict["title"]! as! String))
                    print(dict["title"]!)
                    self.TableData1.append((dict["image_url"]! as! String))
                    print(dict["image_url"]!)
                    self.TableData2.append((dict["tags"]! as! String))
                    print(dict["tags"]!)
                    self.TableData3.append((dict["duration"]! as! String))
                    print(dict["duration"]!)
                    self.TableData4.append((dict["date"]! as! String))
                    print(dict["date"]!)
                    self.TableData5.append((dict["description"]! as! String))
                    print(dict["description"]!)
                    
                    print (self.TableData0.count)
                    self.tableView.reloadData()

                }

            }
            catch
            {
                print("Error with Json: \(error)")
            }
        }
        

        task.resume()
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 105.0;
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        DispatchQueue.main.async
        {
                cell.textLabel!.text = self.TableData0[indexPath.row]
                cell.textLabel!.font = UIFont.systemFont(ofSize: 12.0)
                cell.textLabel!.numberOfLines = 3
        }
        
        
        let imgURL: URL = URL(string: self.TableData1[indexPath.row])!
        let request: URLRequest = URLRequest(url: imgURL)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func display_image()
                {
                    cell.imageView!.image = UIImage(data: data!)
                }
                
                DispatchQueue.main.async
                    {
                        display_image()
                }
            }
            
        })
        
        
        task.resume()
        
        return cell
    
    }

    
    //Method to switch to detail view:
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let defaults = UserDefaults.standard
        defaults.set(self.TableData0[indexPath.row], forKey: "Title")
        defaults.set(self.TableData1[indexPath.row], forKey: "Image")
        defaults.set(self.TableData2[indexPath.row], forKey: "Terms")
        defaults.set(self.TableData3[indexPath.row], forKey: "Time")
        defaults.set(self.TableData4[indexPath.row], forKey: "Date")
        defaults.set(self.TableData5[indexPath.row], forKey: "Desc")
        defaults.synchronize()
        
        
        self.performSegue(withIdentifier: "godetail", sender: self)
        
    }
    
    func do_table_refresh()
    {
        DispatchQueue.main.async(execute:
        {
            self.tableView.reloadData()
            
            return
        })
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    

}
