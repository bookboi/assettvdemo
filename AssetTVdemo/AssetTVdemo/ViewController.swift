//
//  ViewController.swift
//  AssetTVdemo
//
//  Created by Anan Mallik on 11/8/16.
//  Copyright © 2016 Anan Mallik. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBAction func clickLogin(_ sender: Any)
    {
                login()
    }
    

    override func viewDidLoad()
    {
        super.viewDidLoad()

    }
    
    func login()
    {
        if ((username.text == "assettvdemo") && (password.text == "abcd"))
        {
            print("good")
            
            //Switch to view with table data:
            self.performSegue(withIdentifier: "login", sender: self)
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Incorrect Username or Password. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

