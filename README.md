# README #

This README describes the demo app for Asset TV written entirely in Swift 3. 

The app takes in data as JSON and displays it in a table. Each cell can then be clicked for details. 

There is also a temporary login:

username: assettvdemo
password: abcd

Ideally this project can be run on either an iPhone 6 or 7 simulator using XCode 8. 
Later with the correct constraints it can be made to fit all iphone sizes.

Functionality:
The App can Login and then display a list of videos with titles.
Each Title can be clicked to reveal more details about that entry.